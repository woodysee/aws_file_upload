(() => {
    angular.module("Client").controller("UploadVM", UploadVM);

    UploadVM.$inject = ["Upload"];

    function UploadVM (Upload) {
        const vm = this;
        vm.message = "Hello world!";
        vm.uploads = {
            img: null,
            imgS3: null
        };
        vm.status = {
            message: "",
            code: 0
        };
        vm.upload = upload;
        vm.uploadS3 = uploadS3;

        function upload () {
            
            Upload.upload({
                url: '/upload',
                data: {
                    "img-file": vm.uploads.img
                }
            }).then((result)=>{
                vm.fileURL = result.data;
                vm.status.message = "Successful uploading of file";
                vm.status.code = result.status;
            }).catch((error)=>{
                vm.status.message = "Failed to upload file";
                vm.status.code = err.status;
            });
        };

        function uploadS3 () {
            
            Upload.upload({
                url: '/uploadS3',
                data: {
                    "img-file": vm.uploads.imgS3
                }
            }).then((result)=>{
                vm.fileURL = result.data;
                vm.status.message = "Successful uploading of file";
                vm.status.code = result.status;
            }).catch((error)=>{
                vm.status.message = "Failed to upload file";
                vm.status.code = err.status;
            });
        };
    };

})()