require("dotenv").config();
const express = require("express");
const bodyParser = require("body-parser");
const path = require("path");

const NODE_PORT = process.env.NODE_PORT || 3003;

const server = express();

const CLIENT_FOLDER = path.join(__dirname, '/../client');
const MESSAGES_FOLDER = path.join(CLIENT_FOLDER, '/assets/messages');
const routes = require("./routes")(server);

server.use(
    bodyParser.urlencoded({
        extended: false
    })
);
server.use(
    bodyParser.json()
);

server.use(
    express.static(
        __dirname + "/../client/"
    )
);

server.listen(
    NODE_PORT,
    () => {
        console.log("server/server.js: express server is now running locally on port: " + NODE_PORT);
    }
);