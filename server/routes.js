//Import dependencies
const fs = require("fs");
const path = require("path");
//Multer is a decoding library for file transfer
const multer = require("multer");
const AWS = require("aws-sdk");
const multerS3 = require("multer-s3");
const storage = multer.diskStorage({
    destination: './upload_tmp',
    filename: (req, file, callback) => {
        console.log(file);
        callback(null, Date.now()+ '-' + file.originalname);
    }
});

const upload = multer({
    storage: storage
});

AWS.config.region = "ap-southeast-1";
console.log("Initialising AWS");

const s3bucket = new AWS.S3({});

const uploadS3 = new multer({
    storage: multerS3({
        s3: s3bucket,
        bucket: 'woodysee',
        acl: 'public-read',
        metadata: (req, file, cb) => {
            console.log(file.fieldname);
            cb(null, {fieldName: file.fieldname});
        },
        key: (req, file, cb) => {
            console.log(cb);
            console.log(file.originalname);
            cb(null, Date.now() + '-' + file.originalname);
        }
    })
});

//All this is based on Multer's documentation

module.exports = function (server) {
    //To client, i.e. user's local storage in browser
    server.post(
        "/upload", upload.single("img-file"),
        (req, res) => {
            console.log("Uploading file...");
            fs.readFile(req.file.path, (err, data) => {
                if (err) {
                    console.log(err)
                };
                res.status(202).json({
                    size: req.file.size
                });
            })
        }
    );
    //To server, e.g. AWS cloud storage
    server.post(
        "/uploadS3",
        uploadS3.single("img-file"),
        (req, res) => {
            res.send("Successfully uploaded a file " + req.file.size);
        }
    );

};